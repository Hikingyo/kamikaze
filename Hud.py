from kivy.lang import Builder
from kivy.properties import ObjectProperty, NumericProperty, ListProperty
from kivy.uix.widget import Widget


class Hud(Widget):
    Builder.load_file('hud.kv')
    app = ObjectProperty(None)
    battery = NumericProperty(0)
    pitch = NumericProperty(0)
    roll = NumericProperty(0)
    yaw = NumericProperty(0)
    altitude = NumericProperty(0)
    battery_color = ListProperty([0, 0, 0, 1])
    battery_background_color = ListProperty([1, 0, 0, 1])

    def update(self, *largs):
        # print(self.app.drone.navdata)
        navdata = self.app.drone.navdata.get(0, dict())
        battery_level = navdata.get('battery', 0)
        self.battery = battery_level
        self.battery_color = [1 - (battery_level / 100), battery_level / 100, 0, 1]
        self.altitude = navdata.get('altitude', 0)
        self.pitch = navdata.get('theta', 0)
        self.roll = navdata.get('phi', 0)
        self.yaw = navdata.get('psi', 0)
