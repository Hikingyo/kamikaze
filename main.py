import kivy
from kivy.app import App
from kivy.properties import NumericProperty, ReferenceListProperty, Clock
from kivy.uix.widget import Widget
from kivy.vector import Vector

import KeyboardListener
from ARDroneAPI.libARDrone import ARDrone, Log
from Hud import Hud
from KeyBoardCommand import Keyboardcontrol

kivy.require('1.9.1')  # replace with your current kivy version !


class PongBall(Widget):
    velocity_x = NumericProperty(0)
    velocity_y = NumericProperty(0)
    velocity = ReferenceListProperty(velocity_x, velocity_y)

    def move(self):
        self.pos = Vector(*self.velocity) + self.pos


class ARDroneApp(App):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.hud = Hud(app=self)
        self.key_commander = Keyboardcontrol()
        self.drone = ARDrone()

    def build(self):
        key_listener = KeyboardListener.KeyboardListener()
        key_listener.bind(on_key_down=self.on_key_down_callback)
        key_listener.bind(on_key_up=self.on_key_up_callback)
        self.drone.flat_trim()
        self.root = self.hud
        Clock.schedule_interval(self.hud.update, 1.0 / 60.0)

    def on_key_down_callback(self, val, keycode, modifier):
        self.key_commander.get_action(keycode, self.drone)

    def on_key_up_callback(self, val):
        self.drone.hover()

    def on_stop(self):
        self.drone.shutdown()


if __name__ == '__main__':
    Log.info("Kamikaze: Start !!")
    ARDroneApp().run()
