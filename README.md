# Kamikaze

An python application made with [kivyframework][kivy] to control and programm
ARDrone quadcopter by Parrot.
Because it is the beginning, we have many works to make this really playable and affordable.

A part of this software is a python SDK like for the ARDrone and will be more complet is the future
in order to implement all the feature from official SDK.

## Status

This program is under heavy development so some functionality is missing
and the gui is minimalistic

## Requirements

This software is develop and tested only under python 3.4 and kivy 1.9.1.
You should have both install to use it.
See [requirements.txt][2] for further informations

## Installation
Run simply
```bash
python -m pip install -r requirements.txt
```

## Repository

The public repository is located here:
[Kamikaze Repo][1]

## TODOS
- make a super fun GUI
- implement all feature from SDK
- implement controller and joystick
- implement video
- make coffee

## Contribution

Feel free to test, report bugs, submit feature.

If you want to participate, send me a message.

## License
![BEER-WARE LICENSE](https://upload.wikimedia.org/wikipedia/commons/d/d5/BeerWare_Logo.svg)

[kivy]: https://kivy.org
[1]: https://bitbucket.org/Hikingyo/kamikaze
[2]: requirements.txt