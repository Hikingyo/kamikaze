import struct
# @TODO implémenter le transcodage des différentes options. Revoir structure lib ?
# matrix33_t = fffffffff
# vector31_t = fff
# uint32 = I
# float32 = f
# uint16 = HH
from ARDroneAPI import OPTIONS_KEY, TAG_TYPE_NAME

"""
Minimal navdata :
- ctrl_state uint32
- battery uint32
- theta float32
-phi float32
-psi float32
-altitude uint32
-vx float32
-vy float32
-vz float32
-num_frames uint32
"""
navdata_demo_format = "IIfffIfffI"

"""
Vision detect :
- nbDetect uint32 ( max 4)
- type uint32[4]
- xc uint32[4]
- yx uint32[4]
- width uint34[4]
- height uint32[4]
- dist uint32[4]
- orientationAngle float32[4]
- rotation float32 matrix 3*3[4]
- translation float32 vector 3[4}
- cameraSource unint32[4]
"""
vision_detect_format = 'IIIIIIIIIIIIIIIIIIIIIIIIIffffffffffffffffffffffffffffffffffffffffffffffffffffIIII'

"""
checksum for all navdata udp packet, including options
data uint32
"""
checksum_format = 'I'

"""
wifi
link_quality uint32
"""
wifi_format = 'I'


def decode_navdata(packet):
    """
    Decode a navdata packet.
    - Header uint32
    - Dronne state uint32
    - Sequence number uint32
    - Vision flag uint32
    [ -options :
        - id_nr uint 16
        - size uint16
        - data size
    ]
    - checksum :
        - cks id uint16
        - size uint16
        - cks data uint32
    """
    offset = 0
    """
    decode packet for 128 first bits, header, drone state, sequence number and vision flag
    """
    _ = struct.unpack_from("IIII", packet, offset)
    drone_state = dict()
    drone_state['fly_mask'] = _[1] & 1  # FLY MASK : (0) ardrone is landed, (1) ardrone is flying
    drone_state['video_mask'] = _[1] >> 1 & 1  # VIDEO MASK : (0) video disable, (1) video enable
    drone_state['vision_mask'] = _[1] >> 2 & 1  # VISION MASK : (0) vision disable, (1) vision enable */
    drone_state['control_mask'] = _[1] >> 3 & 1  # CONTROL ALGO (0) euler angles control, (1) angular speed control */
    drone_state['altitude_mask'] = _[
                                       1] >> 4 & 1  # ALTITUDE CONTROL ALGO : (0) altitude control inactive (1) altitude control active */
    drone_state['user_feedback_start'] = _[1] >> 5 & 1  # USER feedback : Start button state */
    drone_state['command_mask'] = _[1] >> 6 & 1  # Control command ACK : (0) None, (1) one received */
    drone_state['fw_file_mask'] = _[1] >> 7 & 1  # Firmware file is good (1) */
    drone_state['fw_ver_mask'] = _[1] >> 8 & 1  # Firmware update is newer (1) */
    drone_state['fw_upd_mask'] = _[1] >> 9 & 1  # Firmware update is ongoing (1) */
    drone_state['navdata_demo_mask'] = _[1] >> 10 & 1  # Navdata demo : (0) All navdata, (1) only navdata demo */
    drone_state['navdata_bootstrap'] = _[
                                           1] >> 11 & 1  # Navdata bootstrap : (0) options sent in all or demo mode, (1) no navdata options sent */
    drone_state['motors_mask'] = _[1] >> 12 & 1  # Motor status : (0) Ok, (1) Motors problem */
    drone_state['com_lost_mask'] = _[1] >> 13 & 1  # Communication lost : (1) com problem, (0) Com is ok */
    drone_state['vbat_low'] = _[1] >> 15 & 1  # VBat low : (1) too low, (0) Ok */
    drone_state['user_el'] = _[1] >> 16 & 1  # User Emergency Landing : (1) User EL is ON, (0) User EL is OFF*/
    drone_state['timer_elapsed'] = _[1] >> 17 & 1  # Timer elapsed : (1) elapsed, (0) not elapsed */
    drone_state['angles_out_of_range'] = _[1] >> 19 & 1  # Angles : (0) Ok, (1) out of range */
    drone_state['ultrasound_mask'] = _[1] >> 21 & 1  # Ultrasonic sensor : (0) Ok, (1) deaf */
    drone_state['cutout_mask'] = _[1] >> 22 & 1  # Cutout system detection : (0) Not detected, (1) detected */
    drone_state['pic_version_mask'] = _[
                                          1] >> 23 & 1  # PIC Version number OK : (0) a bad version number, (1) version number is OK */
    drone_state['atcodec_thread_on'] = _[1] >> 24 & 1  # ATCodec thread ON : (0) thread OFF (1) thread ON */
    drone_state['navdata_thread_on'] = _[1] >> 25 & 1  # Navdata thread ON : (0) thread OFF (1) thread ON */
    drone_state['video_thread_on'] = _[1] >> 26 & 1  # Video thread ON : (0) thread OFF (1) thread ON */
    drone_state['acq_thread_on'] = _[1] >> 27 & 1  # Acquisition thread ON : (0) thread OFF (1) thread ON */
    drone_state['ctrl_watchdog_mask'] = _[
                                            1] >> 28 & 1  # CTRL watchdog : (1) delay in control execution (> 5ms), (0) control is well scheduled */
    drone_state['adc_watchdog_mask'] = _[
                                           1] >> 29 & 1  # ADC Watchdog : (1) delay in uart2 dsr (> 5ms), (0) uart2 is good */
    drone_state['com_watchdog_mask'] = _[1] >> 30 & 1  # Communication Watchdog : (1) com problem, (0) Com is ok */
    drone_state['emergency_mask'] = _[1] >> 31 & 1  # Emergency landing : (0) no emergency, (1) emergency */
    data = dict()
    data['drone_state'] = drone_state
    data['header'] = _[0]
    data['seq_nr'] = _[2]
    data['vision_flag'] = _[3]
    offset += struct.calcsize("IIII")
    """
    Parsing UDP packet for options and check sum
    """
    while 1:
        try:
            id_nr, size = struct.unpack_from("HH", packet, offset)
            offset += struct.calcsize("HH")
        except struct.error:
            break
        values = []
        for i in range(size - struct.calcsize("HH")):
            values.append(struct.unpack_from("c", packet, offset)[0])
            offset += struct.calcsize("c")
        # navdata_tag_t in navdata-common.h
        if id_nr == OPTIONS_KEY['DEMO']:
            values = decode_navdata_demo(values)
        if id_nr == OPTIONS_KEY['VISION_DETECT']:
            values = decode_vision_detect(values)
        if id_nr == OPTIONS_KEY['CKS']:
            values = decode_checksum(values)
        if id_nr == OPTIONS_KEY['WIFI']:
            values = decode_wifi(values)
        data[id_nr] = values
    return data


def decode_navdata_demo(values):
    values = struct.unpack_from(navdata_demo_format, b''.join(values))
    ctrl_state = dict()
    ctrl_state['default'] = values[1] & 1
    ctrl_state['init'] = values[1] >> 1 & 1
    ctrl_state['landed'] = values[1] >> 2 & 1
    ctrl_state['flying'] = values[1] >> 3 & 1
    ctrl_state['hovering'] = values[1] >> 4 & 1
    ctrl_state['test'] = values[1] >> 5 & 1
    ctrl_state['trans_takeoff'] = values[1] >> 6 & 1
    ctrl_state['trans_gotofix'] = values[1] >> 7 & 1
    ctrl_state['trans_landing'] = values[1] >> 8 & 1
    ctrl_state['trans_looping'] = values[1] >> 9 & 1
    ctrl_state['trans_no_vision'] = values[1] >> 10 & 1
    ctrl_state['num_state'] = values[1] >> 11 & 1
    values = dict(
        zip(['ctrl_state', 'battery', 'theta', 'phi', 'psi', 'altitude', 'vx', 'vy', 'vz', 'num_frames'],
            values))
    values['ctrl_state'] = ctrl_state
    # convert the millidegrees into degrees and round to int, as they
    # are not so precise anyways
    for i in 'theta', 'phi', 'psi':
        values[i] = int(values[i] / 1000)
    return values


def decode_vision_detect(values):
    targets = {
        0: Target(),
        1: Target(),
        2: Target(),
        3: Target()
    }
    values = struct.unpack_from(vision_detect_format, b''.join(values))
    vision_detect = dict()
    offset = 0
    vision_detect['nb_detect'] = values[offset]
    offset += 1
    for i in range(0, 4, 1):
        targets[i].type = TAG_TYPE_NAME.get(values[offset])
        offset += 1
    for i in range(0, 4, 1):
        targets[i].xc = values[offset]
        offset += 1
    for i in range(0, 4, 1):
        targets[i].yc = values[offset]
        offset += 1
    for i in range(0, 4, 1):
        targets[i].width = values[offset]
        offset += 1
    for i in range(0, 4, 1):
        targets[i].height = values[offset]
        offset += 1
    for i in range(0, 4, 1):
        targets[i].dist = values[offset]
        offset += 1
    for i in range(0, 4, 1):
        targets[i].orientation = values[offset]
        offset += 1
    for i in range(0, 4, 1):
        for j in range(0, 9, 1):
            targets[i].rotation[j] = values[offset]
            offset += 1
    for i in range(0, 4, 1):
        for j in range(0, 3, 1):
            targets[i].translation[j] = values[offset]
            offset += 1
    for i in range(0, 4, 1):
        targets[i].camera_source = values[offset]
        offset += 1
    # values = dict(
    #     zip(['nb_detected', 'type', 'xc', 'yc', 'with', 'height', 'dist', 'orientation_angle', 'rotation',
    #          'translation', 'camera_source'], values)
    # )
    vision_detect['targets'] = targets
    return vision_detect


def decode_checksum(values):
    return struct.unpack_from(checksum_format, b''.join(values))


def decode_wifi(values):
    return struct.unpack_from(wifi_format, b''.join(values))


class Target(object):
    """
    Utility class for items detected by the drone
    """
    xc = 0
    yc = 0
    type = ''
    width = 0
    height = 0
    dist = 0
    orientation = 0
    rotation = dict()
    translation = dict()
    camera_source = 0
