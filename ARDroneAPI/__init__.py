import logging

ARDONE_FTP_PORT = 5551
ARDRONE_AUTH_PORT = 5552
ARDRONE_VIDEO_RECORDER = 5553
ARDRONE_NAVDATA_PORT = 5554
ARDRONE_VIDEO_PORT = 5555
ARDRONE_COMMAND_PORT = 5556
ARDRONE_RAW_CAPTURE_PORT = 5557
ARDONE_PRINTF_PORT = 5558
ARDRONE_CONTROL_PORT = 5559
ARDRONE_IP = "192.168.1.1"

OPTIONS_KEY = {
    'DEMO': 0,
    'TIME': 1,
    'RAW_MEASURES': 2,
    'PHYS_MEASURE': 3,
    'GYROS_OFFSET': 4,
    'EULER_ANGLES': 5,
    'REFERENCES': 6,
    'TRIMS': 7,
    'RC_REFERENCES': 8,
    'PWM': 9,
    'ALTITUDE': 10,
    'VISION_RAW': 11,
    'VISION_OF': 12,
    'VISION': 13,
    'VISION_PERF': 14,
    'TRACKERS_SEND': 15,
    'VISION_DETECT': 16,
    'WATCHDOG': 17,
    'ADC_DATA_FRAME': 18,
    'VIDEO_STREAM': 19,
    'GAMES': 20,
    'PRESSURE_RAW': 21,
    'MAGNETO': 22,
    'WIND_SPEED': 23,
    'KALMAN_PRESSURE': 24,
    'HDVIDEO_STREAM': 25,
    'WIFI': 26,
    'ZIMMU_3000': 27,  # deprecated
    'CKS': 65535
}

# TODO vérifier s'il ne faut pas transformer id en sequence cf Soft/Common/led_animation.h
LED_ANIMATION_IDS = {
    'blinkGreenRed': 0,
    'blinkGreen': 1,
    'blinkRed': 2,
    'blinkOrange': 3,
    'snakeGreenRed': 4,
    'fire': 5,
    'standard': 6,
    'red': 7,
    'green': 8,
    'redSnake': 9,
    'blank': 10,
    'rightMissile': 11,
    'leftMissile': 12,
    'doubleMissile': 13,
    'frontLeftGreenOtherRed': 14,
    'frontRightGreenOtherRed': 15,
    'rearRightGreenOtherRed': 16,
    'rearLeftGreenOtherRed': 17,
    'leftGreenRightRed': 18,
    'RightGreenLeftRed': 19,
    'blinkStandard': 20
}

# maybe use CAD_type instead cf Soft/Common/ardron_api.h
TAG_TYPE_NAME = {
    0: 'none',
    1: 'shell_tag',
    2: 'roundel',
    3: 'oriented_roundel',
    4: 'stripe',
    5: 'cap',
    6: 'shell_tag_v2',
    7: 'tower_side',
    8: 'black_roundel',
    9: 'num'
}

TAG_TYPE = {
    'none': 0,
    'shell_tag': 1,
    'roundel': 2,
    'oriented_roundel': 4,
    'stripe': 4,
    'cap': 5,
    'shell_tag_v2': 6,
    'tower_side': 7,
    'black_roundel': 8,
    'num': 9
}

DETECTION_SOURCE_CAMERA = {
    'CAMERA_HORIZONTAL': 0,  # Tag was detected on the front camera
    'CAMERA_VERTICAL': 1,  # Tag was detected on the vertical camera at full speed
    'CAMERA_VERTICAL_HSYNC': 2,  # Tag was detected on the vertical camera inside the horizontal pipeline
    'CAMERA_NUM': 3
}

# if using kivy framework, set logger to kivy logger, else set own logger
try:
    from kivy.logger import Logger

    Log = Logger
except ImportWarning:
    Log = logging.getLogger('ARDroneAPI')
    Log.setLevel(logging.DEBUG)

    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.DEBUG)

    formatter = logging.Formatter('Kamikaze - %(asctime)s - %(name)s - %(levelname)s - %(message)s')
    console_handler.setFormatter(formatter)

    Log.addHandler(console_handler)
