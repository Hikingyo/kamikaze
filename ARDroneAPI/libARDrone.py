"""This file define the ARDrone object"""
import multiprocessing
import threading

from ARDroneAPI import libARNetwork, Log
from ARDroneAPI.libAT import *

__author__ = "Hikingyo"


class ARDrone(object):
    """ARDrone Class.
    This class repsresent ARDron and permit to control it
    and receive video and navdata
    """

    def __init__(self):
        Log.debug("ARDrone: Start network threading")
        # Start command sequence
        self.seq_nb = 1
        # Init watchdog to keep wifi w/ drone
        self.timer_t = 0.2
        self.com_watchdog_timer = threading.Timer(self.timer_t, self.commwdg)
        self.lock = threading.Lock()
        self.speed = 0.2
        # Config drone nav data mode eg : true = minimal
        self.at(at_config, "general:navdata_demo", "TRUE")
        self.nav_pipe, nav_pipe_other = multiprocessing.Pipe()
        # Command thread
        self.command_pipe, command_other_pipe = multiprocessing.Pipe()
        # Network process
        self.network_process = libARNetwork.ARDroneNetworkProcess(nav_pipe_other)
        self.network_process.start()
        # IPC thread
        self.ipc_thread = libARNetwork.IPCThread(self)
        self.ipc_thread.start()
        # Navdata
        self.navdata = dict()
        self.time = 0
        # Online status
        self.is_online = 0

    def at(self, cmd, *args, **kwargs):
        """Wrapper for the libAT api

        This method take care that the sequence number is properly increase after each
        command and the watchdog timer is started to avoid drone discnonnected
        """
        self.lock.acquire()
        self.com_watchdog_timer.cancel()
        cmd(self.seq_nb, *args, **kwargs)
        self.seq_nb += 1
        self.com_watchdog_timer = threading.Timer(self.timer_t, self.commwdg)
        self.com_watchdog_timer.start()
        self.lock.release()

    def takeoff(self):
        if self.navdata['drone_state']['fly_mask'] == 1:
            return
        Log.debug("ARDrone: takeoff")
        # Set horizon
        self.at(at_ftrim)
        # Configure the drone control
        self.at(at_config, "control:altitude_max", "20000")
        self.at(at_ref, True)

    def land(self):
        if self.navdata['drone_state']['fly_mask'] == 0:
            return
        self.at(at_ref, False)

    def hover(self):
        self.at(at_pcmd, False, 0, 0, 0, 0)

    def move_left(self):
        self.at(at_pcmd, True, -self.speed, 0, 0, 0)

    def move_right(self):
        self.at(at_pcmd, True, self.speed, 0, 0, 0)

    def move_up(self):
        self.at(at_pcmd, True, 0, 0, self.speed, 0)

    def move_down(self):
        self.at(at_pcmd, True, 0, 0, -self.speed, 0)

    def move_forward(self):
        self.at(at_pcmd, True, 0, -self.speed, 0, 0)

    def move_backward(self):
        self.at(at_pcmd, True, 0, self.speed, 0, 0)

    def turn_left(self):
        self.at(at_pcmd, True, 0, 0, 0, -self.speed)

    def turn_right(self):
        self.at(at_pcmd, True, 0, 0, 0, self.speed)

    def emergency(self):
        """Toggle the drone emergency mode"""
        self.at(at_ref, False, True)
        self.at(at_ref, False, False)

    def flat_trim(self):
        """ tell to the drone is lying horizontaly"""
        self.at(at_ftrim)

    def set_speed(self, speed):
        """
        Set the drone's speed
        :param speed: float [0..1]
        :return: void
        """
        if 0 <= speed <= 1:
            self.speed = float(speed)

    def calib(self):
        """
        Asks the drone to calibrate the magnetometer.
        This command MUST be sent when drone is flying
        :return: void
        """
        if self.navdata['drone_state']['fly_mask'] == 1:
            self.at(at_calib)

    def commwdg(self):
        """
        Communication watchdog signal
        This needs to be send regulary to keep the communication w/ the drone
        """
        self.at(at_commwdg)

    def shutdown(self):
        """
        Shut down the drone.

        This methode does not land or turn off the drone
        but the communication, close all socket, pipes
        and threads of the current object.
        :return: void
        """
        self.lock.acquire()
        self.com_watchdog_timer.cancel()
        self.network_process.stop.set()
        # self.network_process.join()
        self.ipc_thread.stop.set()
        # self.ipc_thread.join()
        self.lock.release()

    def move(self, lr, fb, vs, va):
        """
        Makes the drone move in multiple direction at same time
        :param lr: left/right tilt
        :param fb: front/back tilt
        :param vs: verticla speed
        :param va: spin speed
        :return: void
        TODO: check magneto config and set the right at pcmd
        """
        self.at(at_pcmd, True, lr, fb, vs, va)

    def led_anim(self, name, freq, duration):
        """
        Start a led animation by name
        :param name: name of animation
        :param freq: frequency of animation
        :param duration: duration of animation
        :return: void
        """
        self.at(at_animate_leds, name, freq, duration)
