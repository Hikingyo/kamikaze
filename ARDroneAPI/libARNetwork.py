import select
import socket
import threading

from ARDroneAPI import libNavData, ARDRONE_NAVDATA_PORT, ARDRONE_IP, Log


class ARDroneNetworkProcess(threading.Thread):
    """
    ARDrone Network process

    This process collects data from the drone, convert
    and sends it ti the IPCThread.
    """
    Log.debug("ARDroneNetworkProcess: Start network threading")
    stop = threading.Event()

    def __init__(self, nav_pipe):
        threading.Thread.__init__(self)
        self.nav_pipe = nav_pipe

    def run(self):
        nav_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        nav_socket.setblocking(0)
        nav_socket.bind(('', ARDRONE_NAVDATA_PORT))
        nav_socket.sendto(bytes("\x01\x00\x00\x00", 'ASCII'), (ARDRONE_IP, ARDRONE_NAVDATA_PORT))
        # Listener loop
        while not self.stop.is_set():
            inputready, outready, exceptready = select.select([nav_socket], [], [])
            for i in inputready:
                if i == nav_socket:
                    while 1:
                        try:
                            data = nav_socket.recv(65535)
                        except IOError:
                            # We consumed every packet from the socket and continue with the last one
                            break
                    navdata = libNavData.decode_navdata(data)
                    self.nav_pipe.send(navdata)
        nav_socket.close()


class IPCThread(threading.Thread):
    """Inter process communication Thread

    This thread collect the data from the ARDroneNetWorkProcess and
    forwards it to the ARDrone.
    """
    stop = threading.Event()

    def __init__(self, drone):
        threading.Thread.__init__(self)
        self.drone = drone

    def run(self):
        _navdata = dict()
        while not self.stop.is_set():
            while self.drone.nav_pipe.poll():
                _navdata = self.drone.nav_pipe.recv()
            self.drone.navdata = _navdata

