import socket
import struct

from ARDroneAPI import ARDRONE_IP, ARDRONE_COMMAND_PORT, LED_ANIMATION_IDS


def at(command, seq, params):
    """
    :param command: the command to send
    :param seq: the sequence number
    :param params: list of params to send with command
    :return: void
    """
    param_str = ''
    for param in params:
        if type(param) == int:
            param_str += ",%d" % param
        elif type(param) == float:
            param_str += ",%d" % float_to_int(param)
        elif type(param) == str:
            param_str += ',"' + param + '"'
    message = "AT*%s=%i%s\r" % (command, seq, param_str)
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.sendto(bytes(message, 'ASCII'), (ARDRONE_IP, ARDRONE_COMMAND_PORT))


def at_config(seq, option, value):
    """Set configuration parameter of the drone"""
    at("CONFIG", seq, [str(option), str(value)])


def at_ref(seq, takeoff, emergency=False):
    """
    Controls the basic behavior of the drone

    :param seq: sequence number of the command.
    :param takeoff: True = takeoff, False = landing.
    :param emergency: set the drone to emergency mode.
    :return: void
    """
    order = 0b10001010101000000000000000000
    if takeoff:
        order += 0b1000000000
    if emergency:
        order += 0b0100000000
    at("REF", seq, [order])


def at_ftrim(seq):
    """
    Tell the drone is lying horizontaly
    :param seq: sequence number
    :return: void
    """
    at("FTRIM", seq, [])


def at_pcmd(seq, progressive, lr, fb, vs, va):
    """
    Moves the drone
    :param seq: sequence number
    :param progressive: True : enable progressive commands
    :param lr: left/right tilt [-1..1] <0 : left, >0 : right
    :param fb: front/back tilt [-1..1] <0 : forwards, >0 : backwards
    :param vs: vertical speed : [-1..1] <0 : go down, >0 rise
    :param va: angular speed : [-1..1] <0 spin left, >0 spin right
    :return: void
    """
    p = 1 if progressive else 0
    at("PCMD", seq, [p, float(lr), float(fb), float(vs), float(va)])


def at_calib(seq):
    """
    Calibrate the magnetometer by spinning around for a few time
    This command MUST be send during flight
    :param seq: sequence number
    :return: boid
    """
    at("CALIB", seq, [0])


def at_commwdg(seq):
    """
    Reset communication watchdog to maintain wifi w/ drone
    FIXME: see if sequence number is useless
    :param seq: sequence number
    :return: void
    """
    at("COMWDG", seq, [])


def at_animate_leds(seq, name, freq, duration):
    """
    Get id of animation, default 0 eg blinkGreenRed
    :param seq: sequence number
    :param id: id of led animation
    :param freq: frequency of animation
    :param duration: duration of animation in second
    :return: void
    """
    animation_id = LED_ANIMATION_IDS.get(name, 0)
    # animation_pattern = LED_ANIMATION_PATTERN.get(animation_id) ??
    at("LED", seq, [animation_id, float(freq), duration])


def float_to_int(f):
    """
    Convert float value to signed inter cf IEEE-754
    :param f: float to convert
    :return: signed integer
    """
    return struct.unpack('i', struct.pack('f', f))[0]
