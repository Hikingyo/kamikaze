import kivy
from kivy.core.window import Window
from kivy.uix.widget import Widget

kivy.require('1.9.1')


class KeyboardListener(Widget):
    def __init__(self, **kwargs):
        self.register_event_type('on_key_down')
        self.register_event_type('on_key_up')
        super(KeyboardListener, self).__init__(**kwargs)
        self._keyboard = Window.request_keyboard(
            self._keyboard_closed, self, 'text')
        if self._keyboard.widget:
            pass
        self._keyboard.bind(on_key_down=self._on_key_down)
        self._keyboard.bind(on_key_up=self._on_key_up)

    def _keyboard_closed(self):
        print('Keyboard closed !')
        self._keyboard.unbind(on_key_down=self._on_key_down)
        self._keyboard.unbind(on_key_up=self._on_key_up)
        self._keyboard = None

    def _on_key_down(self, keyboard, keycode, text, modifiers):
        self.dispatch('on_key_down', keycode, modifiers)

        # Keycode is composed of an integer + a string
        # If we hit escape, release the keyboard
        # if keycode[1] == 'escape':
        #     keyboard.release()

        # Return True to accept the key. Otherwise, it will be used by
        # the system.
        return True

    def _on_key_up(self, keyboard, keycode):
        self.dispatch('on_key_up')
        return True

    def on_key_down(self, keycode, modifier):
        pass

    def on_key_up(self):
        pass
