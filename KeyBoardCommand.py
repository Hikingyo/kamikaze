"""
Binding key to ARDrone command
"""


class Keyboardcontrol(object):
    commands = {
        'z': 'move_forward',
        's': 'move_backward',
        'q': 'move_left',
        'd': 'move_right',
        'spacebar': 'takeoff',
        'up': 'move_up',
        'down': 'move_down',
        'left': 'turn_left',
        'right': 'turn_right',
        'l': 'land',
        'b': 'emergency',
        # 'm': 'led_anim' # on test
    }

    def get_action(self, keycode, drone):
        command = (self.commands.get(keycode[1], None))
        if command is not None:
            return getattr(drone, self.commands.get(keycode[1]))()
